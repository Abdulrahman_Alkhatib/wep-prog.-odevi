﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Alisveris.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "websitenin tarifi";
            return View();
        }

        
        public ActionResult Kategori()
        {
            ViewBag.Message = "Kategoriler sayfasi.";

            return View();
        }
        public ActionResult Marka()
        {
            ViewBag.Message = "Markalar sayfasi.";

            return View();
        }
    }
}
﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Alisveris.Startup))]
namespace Alisveris
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

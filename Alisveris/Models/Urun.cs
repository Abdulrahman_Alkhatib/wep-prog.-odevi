﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Alisveris.Models
{
    public class Urun
    {
        public int id { get; set; }
        public string isim { get; set; }
        public double fiyat { get; set; }
        public string tarif { get; set; }
        public int stok { get; set; }
    }
}
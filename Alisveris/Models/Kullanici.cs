﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Alisveris.Models
{
    public class Kullanici
    {
        public int seviye { get; set; }
        public int id { get; set; }
        public string ad { get; set; }
        public string soyad { get; set; }
        public string email { get; set; }
    }
}